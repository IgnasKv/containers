﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Defense3
{
    class Land
    {
        public string address { get; set; }
        public int area { get; set; }

        //Creating constructor methods
        public Land()
        {
            address = "";
            area = 0;
        }

        public Land(string address, int area)
        {
            this.address = address;
            this.area = area;
        }

        // Creating override method for toString method
        public override string ToString()
        {
            string line = string.Format("|{0, -16}|{1, 7}|", this.address, this.area );

            return line;
        }

    }
}
