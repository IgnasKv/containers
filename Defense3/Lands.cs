﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Defense3
{    
    class Lands
    {
        const int max = 100;

        Land[] lands = new Land[max];
        public int nOfLands { get; set; }

        public Land GetLand(int index)
        {
            return lands[index];
        }

        // Creates a new land
        public void Set(Land l1)
        {
            lands[nOfLands++] = l1;
        }

        // Changes the land at the index
        public void Set(int index, Land l1)
        {
            lands[index] = l1;
        }

        public int operator +(Land first, Land second)
        {
            return first.area + second.area;
        }
    }
}
