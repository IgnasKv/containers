﻿using System;
using System.IO;

namespace Defense3
{
    class Program
    {
        static void Main(string[] args)
        {
            string data = "../../../Data.txt";
            Lands lands = new Lands();
            Read(data, lands);
            Print(lands);
        }

        static void Read(string data, Lands lands)
        {
            using (StreamReader sr = new StreamReader(data))
            {
                string line;

                while ( (line = sr.ReadLine()) != null)
                {
                    string[] parts = line.Split(' ');
                    Land newLand = new Land(parts[0], Convert.ToInt32(parts[1]));
                    lands.Set(newLand);
                }
            }
        }

        static void Print(Lands lands)
        {
            Console.WriteLine("Information about the lands");

            for (int i = 0; i < lands.nOfLands; i++)
            {
                Console.WriteLine( lands.GetLand(i).ToString() );
            }
        }
    }
}
